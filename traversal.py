# name: Sri Padala
import sys

def convert_to_function(given_string):
    temp = given_string
    i = 0
    for item in temp:
        item = "-" + item
        item = item.replace("+","*")
        temp[i] = "(" + item + ")"
        i = i+1

    final_answer = ''
    j = 1
    for item in temp:
        if( j == 1 ):
            final_answer = final_answer + item
            j = j+1
        else:
            final_answer = final_answer +" * "+ item

    final_answer = final_answer.replace("-x","a")
    final_answer = final_answer.replace("x","-x")
    final_answer = final_answer.replace("a","x")
    final_answer = final_answer.replace("-y","b")
    final_answer = final_answer.replace("y","-y")
    final_answer = final_answer.replace("b","y")
    print(final_answer)



def main():
    filename = sys.argv[-1]#getting filename via command line as a command line arguement.
    with open(filename) as f:
        content = f.readlines()
        content = [x.strip() for x in content]

    stuff = content
    for i in range(len(content)):
        stuff[i] = stuff[i].replace(',','')
    
    

    x = 0

    for i in range(len(stuff)):
        if  i > 0:
            if stuff[i][2] == '0':
                x = x+1

    final = ''
    exp = [''] * x

    y = 0
    for i in range(len(stuff)):
        
        x_part = ''
        y_part = ''

        if  i > 0:
            if stuff[i][2] == '0':
               

                if(stuff[i][0]) == '1':
                    x_part = "-x"
                else:
                    x_part = "x"
                if(stuff[i][1]) == '1':
                    y_part = "+-y"
                else:
                    y_part = "+y"
                exp[y] = "(" + x_part + y_part +")"
                
                final = final +  exp[y]
                y = y + 1

         
     
    convert_to_function(exp)  
 
    
if __name__ == "__main__":
    main()