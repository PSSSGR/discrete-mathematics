# Filename:main.py
# Name:Sri Padala

import sys
import math
def evaluate(op1,op2,ch):
    if ch == '*':
        return(op2 * op1)
    elif ch == '/':
        return(op2 / op1)
    elif ch == '+':
        return(op2 + op1)
    elif ch == '-':
        return(op2 - op1)
    elif ch == '^':
        return math.pow(op2,op1)
    else:
        print("Invalid Operator")

def PrintPostfix(given_string,size):
    opStack = list()
    i = 0
    ch = ""
    val = 0
    while i < size:
        ch = given_string[i]
        if ch.isdigit():
            temp = float(ch)
            opStack.append(temp)
        else:
            op1 = opStack[len(opStack)-1]
            opStack.pop()
            op2 = opStack[len(opStack)-1]
            opStack.pop()
            val = evaluate(op1,op2,ch)
            opStack.append(val)
        i = i + 1
    print(val)

def PrintPrefix(given_string,size):
    opStack = list()
    i = size - 1
    ch = ""
    val = 0
    while i >= 0:
        ch = given_string[i]
        if ch.isdigit():
            temp = float(ch)
            opStack.append(temp)
        else:
            op1 = opStack[len(opStack)-1]
            opStack.pop()
            op2 = opStack[len(opStack)-1]
            opStack.pop()
            val = evaluate(op2,op1,ch)
            opStack.append(val)
        i = i - 1
    print(val)
        





def main():
    filename = sys.argv[-1]#getting filename via command line as a command line arguement.
    with open(filename) as f:
        content = f.readlines()
        content = [x.strip() for x in content]
    

    for i in range(len(content)):#checking for prefix or postfix
        if content[i][0].isdigit():
            given_string = content[i]
            given_string = given_string.split()
            size = len(given_string)
            PrintPostfix(given_string,size)
        else:
            given_string = content[i]
            given_string = given_string.split()
            size = len(given_string)
            PrintPrefix(given_string,size)
    
main()