/*
Name:Sri Padala.
Description:Non deterministic finite state automata, to verify if a given string is accepted by machine.
*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <set>
#include <algorithm>
#include <typeinfo>

class NFA{//Holds data for the nfa objects.
    public:
        std::string string_to_manipulate;
        size_t counter = 0;
        std::string *arr_string;
        void give_states(); 
        void set_fields(); 
};



std::string find_the_nextstate(std::string present_state,std::string given_letter,std::vector<std::string> given_transitions){
    std::vector<std::string> temp;
    int j = 0;
    std::string found;
    for(unsigned int i = 0; i < given_transitions.size();i++){
        std::string temp2 = given_transitions[i].substr(1,2);
        std::string temp3 = given_transitions[i].substr(9,1);
        if(present_state == temp2 && given_letter == temp3){//the main logic works here.
            std::string temp4 = given_transitions[i].substr(5,2); 
            if(j == 0){
                found = temp4;
            }else{
                found = found + " " + temp4;
            }
            j++;
        }
    }
    if(found.empty()){//if nothing is found it returns "none_found"
        found = "none_found";
    }else{
        std::replace(found.begin(), found.end(), ' ', ',');//replaces spaces with ",".
    }
    return(found);
}

void removespace(std::string& final_states){
    int count = 0; 
    for (unsigned int i = 0; i < final_states.size(); i++) {
        if (final_states[i] != ' ') {
            final_states[count++] = final_states[i];
        }
        final_states = final_states.substr(0,2); 
    }
}


int main(int argc, char* argv[]) {

    if(argc > 1){
        std::ifstream in;
        std::string x2;
        std::string arr[100];
        std::vector<char> cal_states;
        std::vector<std::string> cal_final_states;
        std::vector<std::string> given_state_transitions;
        int num = 0;
        unsigned int counter1,counter2,cnt,flag1,flag2;
        std::string input;
        std::string str;
        in.open(argv[1]);

        while (std::getline(in, x2)){//getting data from file.
            arr[num] = x2;
            num++;
        }
        std::string message_to_work = arr[0];//message.
        std::string final_states = arr[1];// final states
        std::stringstream ss(final_states);

        while( ss.good() )
        {
            std::string substr;
            std::getline( ss, substr, ',' );
            cal_final_states.push_back( substr );
        }

        for (unsigned int i=0; i< cal_final_states.size(); i++){
            removespace(cal_final_states[i]);
        }
        for(int x = 2; x < num;x++){
            given_state_transitions.push_back(arr[x]);
        }

        unsigned int final_states_size = cal_final_states.size();

        for(int x = 2; x < num;x++){
            cal_states.emplace_back(arr[x][2]);
            cal_states.emplace_back(arr[x][6]);
        }

        std::set<char> s( cal_states.begin(), cal_states.end() );
        cal_states.assign( s.begin(), s.end() );
        unsigned int total_num_states = s.size();
        std::vector<std::string> states;
        std::vector<std::string> msg_states;

        for(int j = 2; j < num; j++){//to get the total states and string alphabets.
            arr[j].replace(0,1," ");
            arr[j].replace(10,1," ");
            std::string temp = arr[j].substr(1,2);
            std::string temp2 = arr[j].substr(5,2);
            std::string temp3 = arr[j].substr(9,1);
            states.push_back(temp);
            states.push_back(temp2);
            msg_states.push_back(temp3);
        }

        std::set<std::string> set_one( msg_states.begin(), msg_states.end() );//converting vector to set as it will delete duplicates and sorts.
        msg_states.assign( set_one.begin(), set_one.end() );

        std::set<std::string> set_two( states.begin(), states.end() );//converting vector to set as it will delete duplicates and sorts.
        states.assign( set_two.begin(), set_two.end() );

        std::vector<std::string> output(set_two.begin(), set_two.end()); 
        std::vector<std::string> output2(set_one.begin(), set_one.end()); 

        std::string *states_present;
        std::string *final_states_1;
        std::string *alphabet_to_work;
        states_present = new std::string[total_num_states];
        final_states_1 = new std::string[final_states_size];

        for (unsigned int b = 0; b < total_num_states; b++){
                states_present[b] = output.at(b);
        }

        for (unsigned int b = 0; b < final_states_size; b++){
                final_states_1[b] = cal_final_states[b];
        }

        unsigned int num_of_alphabets = output2.size();
        alphabet_to_work = new std::string[num_of_alphabets];

        for (unsigned int b = 0; b < num_of_alphabets; b++){
                alphabet_to_work[b] = output2[b];
        }
        std::vector<char> alphabets;
        char helper = 'a';//to get the alphabet string.
        for (unsigned int b = 0; b < num_of_alphabets; b++){
            alphabets.push_back(helper);
            helper = helper + 1;
        }

        NFA** trans;//creating a 2-D object.
        trans = new NFA*[total_num_states];

        for (unsigned int j = 0; j < total_num_states; j++){
            trans[j] = new NFA[num_of_alphabets];
        }

        for (unsigned int i = 0; i < total_num_states; i++) {//intialize the objects.
            for (unsigned int j = 0; j < num_of_alphabets; j++){
                    std::string temp = find_the_nextstate(states_present[i],alphabet_to_work[j],given_state_transitions);
                    trans[i][j].string_to_manipulate = temp;
                    trans[i][j].give_states();
                }
        }

        unsigned int current_states,temp_states;
        std::string current[total_num_states],temp[total_num_states];


        input = message_to_work;
        if (input.empty()){
            exit(0);
        }
        current_states = 0;
        current[current_states] = states_present[0];
        for (unsigned int i = 0; i < input.length(); i++){
            for (counter1 = 0; counter1 < num_of_alphabets; ++counter1){//get the counter when there is a match.
                if (alphabets[counter1] == input[i]){
                    break;
                }
            }
            temp_states = 0;
            for (unsigned int k = 0; k <= current_states; k++) { 
                    if (current[k] == "none_found"){
                        continue;
                    }
                for (counter2 = 0; counter2 < total_num_states; counter2++){
                        if (states_present[counter2] == current[k]){
                            break;
                        }
                }
                cnt = trans[counter2][counter1].counter;
                for (unsigned int y = 0; y < cnt; y++){
                    str = trans[counter2][counter1].arr_string[y];
                    flag1 = 0;
                    for (unsigned int z = 0; z < temp_states; z++){ 
                        if (str == "none_found") {
                            flag1 = 1;
                            break;
                        }
                        if (temp[z] == str) {
                            flag1 = 1;
                            break;
                        }
                    }
                    if (flag1 == 0){
                        temp[temp_states++] = str;
                    }
                }
            }
            for (unsigned int y = 0; y <= temp_states; y++){
                current[y] = temp[y];
            }
            current_states = temp_states - 1;
        }
        flag2 = 0;
        for (unsigned int i = 0; i <= current_states; i++) {//finally checks the strings and gives the answer.
            for(unsigned int j = 0; j < final_states_size; j++){
                if (final_states_1[j] == current[i]) {
                    std::cout<<"yes"<<std::endl;
                    flag2 = 1;
                    break;
                }
            }
            if (flag2 == 1){
                break;
            }
        }
        if (flag2 == 0){
            std::cout<<"no"<<std::endl;
        }
    }else{
        std::cout<<"Please give an input file to read data from"<<"\n";
    }
    return 0;
}

void NFA::give_states(){
    set_fields();
    char *states = new char[string_to_manipulate.length() + 1];
    strcpy(states, string_to_manipulate.c_str());
    char *p = strtok(states, ",");
    int i = 0;
    arr_string = new std::string[counter];
    while (p != 0) {
        arr_string[i] = p;
        i++;
        p = strtok(NULL, ",");
    }
}

void NFA::set_fields(){
    counter = 1;
    size_t found = string_to_manipulate.find(',');
    while (found != std::string::npos) {
        counter++;
        found = string_to_manipulate.find(',', found + 1);
    }
}